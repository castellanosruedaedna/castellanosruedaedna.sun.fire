<!DOCTYPE html>
  <head>
  <?php
	require '../app/vendor/autoload.php';
	$app = new \Slim\Slim();
	$app->config(array(
    'templates.path' => '../app/templates/'
	));
	$app->container->singleton('db', function () {
	return new PDO("pgsql:host=148.208.228.139 user=a10160916 password=Edna"#04 dbname=a10160916");
	});

	require '../app/routes/root.php';
	require '../app/routes/xml.php';
	$app->run();
		php?>
		
    <meta charset="utf-8" />
    <title>Inicio</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/main.css">
	<script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  </head>
  <body>
    <h1>Censo de Población y Vivienda 2010</h1>
    <hr />
    <h2>Sobre la API</h2>
    <p>
       La API surge a partir de los datos del <strong>INEGI </strong> 
	   para tener un mejor acceso de la información de los datos generados
	   a partir de los censos que de se hicieron en dicho año.
    </p>
    <p>
       Para información sobre como hacer uso de la API, <a href="doc/">Clic para acceder a la documentación</a>.
    </p>
    <hr />
    <h2>Datos del equipo</h2>
    <p>
       <strong> Equipo 2 </strong>
	   <ul>
			<li> Castellanos Rueda Edna </li>
			<li> Pérez García Javier </li>
			<li> Pinacho López Gabriela Janeth </li>
			<li> Quiroz Chavez Valentina </li>
			<li> Ramírez Hernández Luis Ángel </li>
			<li> Ruiz López Gustabo Adam </li>
			<li> Vasquez Pérez Luis Alberto </li>
		</ul>
    </p>
   <hr />
	<script src="js/vendor/jquery-1.11.0.min.js"></script>
  	<script src="js/vendor/bootstrap.min.js"></script>
  	<script src="js/api.js"></script>
  </body>
</html>
