<!DOCTYPE html>
  <head>
    <meta charset="utf-8" />
    <title>Documentación de API</title>
	<meta name="viewport" content="width=device-width, 
initial-scale=1">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="../css/main.css">
	<script 
src="../js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  </head>
  <body>
    <a href="../"><strong><------- Volver</strong></a>
    <hr />
    <h1>Documentación de API <strong> Censo de población y vivienda 2010 
</strong> </h1>
	<blockquote>
    <p class="lead">
      La API se basa en los principios de REST y expone los datos 
obtenidos por El Insituto Nacional de Estadistica 
	  y Geografia, para saber el comportamiento de la población, así 
como en las condiciones en las que se viven.
    </p>
    <p class="lead">
      La URL raiz es <a 
href="http://castellanosruedaedna.sun.fire/api/">http://castellanosruedaedna.sun.fire/api/</a>.
    </p>
	</blockquote>
    <hr />

    <h2 class="bg-info"> <strong>Información </strong></h2>
	<blockquote>
    <p>Descripción:
	<footer>Obtener información total de los datos</footer>
	</p>
	</blockquote>
    <ul>
	<p class="lead">
      <li><strong>URL</strong>: 
http://castellanosruedaedna.sun.fire/api/xml</li>
      <li><strong>Metodo HTTP</strong>: GET</li>
      
      <li><strong>Respuesta</strong>: XML</li>
      <li><strong>Ejemplo</strong>:
	</p>
<pre>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
       &lt;?catalogo?&gt;
	&lt;?id_entidad id="confidencial"?&gt;?
         &lt;?id_municipio?&gt;
		&lt;?id_tema1?&gt; 
		&lt;?id_tema2?&gt; 
		&lt;?id_tema3?&gt;
         &lt;?id?&gt;
       &lt;?cantidad?&gt;

      </pre></li>
    </ul>
	
	<h2 class="bg-info"> <strong> Indicadores de datos </strong> 
</h2>
	<blockquote>
    <p>Descripción: 
		<footer>Se mostrará los principales indicadores de 
información nacional </footer> 
	</p>
	</blockquote>
    <ul>
	<p class="lead">
      <li><strong>URL</strong>: 
http://castellanosruedaedna.sun.fire/</li>
      <li><strong>Metodo HTTP</strong>: GET</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>
	 </p>
<pre>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;?catalogo?&gt;
   &lt;?id?&gt;id="confidencial"?&gt;
     &lt;?id_identificador?&gt;
     &lt;?desc_identificador?&gt;
</pre>
      </li>
    </ul>
	
	<h2 class="bg-info"> <strong> Entidad</strong> </h2>
	<blockquote>
    <p>Descripción: 
		<footer>Se muestra la informacióin de las 32 entidades 
del país,
				pero solo almacena la información 
Nacional por indicaciones
		</footer> 
	</p>
	</blockquote>
    <ul>
	<p class="lead">
      <li><strong>URL</strong>: 
http://castellanosruedaedna.sun.fire/</li>
      <li><strong>Metodo HTTP</strong>: GET</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>
	 </p>
	<pre>	&lt;?xml version="1.0" encoding="UTF-8"?&gt;
			&lt;?catalogo?&gt;
			&lt;?id_entidad?&gt;id="confidencial"?&gt;
			&lt;?clave_entidad?&gt;
			&lt;?nombre_entidad?&gt;
	</pre>
      </li>
    </ul>
	
	<h2 class="bg-info"> <strong> Municipio </strong> </h2>
	<blockquote>
    <p>Descripción: 
		<footer>Se muestra la informacióin de cada uno de los 
municipios que se encuentran dentro de las entidades
				del país, pero solo se almacenan los de 
Nacional
		</footer> 
	</p>
	</blockquote>
    <ul>
	<p class="lead">
      <li><strong>URL</strong>: 
http://castellanosruedaedna.sun.fire/</li>
      <li><strong>Metodo HTTP</strong>: GET</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>
	 </p>
	<pre>	&lt;?xml version="1.0" encoding="UTF-8"?&gt;
			&lt;?catalogo?&gt;
			&lt;?id_municipio?&gt;id="confidencial"?&gt;
			&lt;?clave_municipio?&gt;
			&lt;?nombre_municipio?&gt;
			&lt;?id_entidad?&gt;
	</pre>
      </li>
    </ul>
	
	<h2 class="bg-info"> <strong> Tema_Nivel1 </strong> </h2>
	<blockquote>
    <p>Descripción: 
		<footer>Los indicadores se encuentran organizados por 
niveles, dentro de este primer nivel, se encuentran
				población, hogares, viviendas, así como 
sociedad y gobierno.
		</footer> 
	</p>
	</blockquote>
    <ul>
	<p class="lead">
      <li><strong>URL</strong>: 
http://castellanosruedaedna.sun.fire/</li>
      <li><strong>Metodo HTTP</strong>: GET</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>
	 </p>
	<pre>	&lt;?xml version="1.0" encoding="UTF-8"?&gt;
			&lt;?catalogo?&gt;
			&lt;?id_tema1?&gt;id="confidencial"?&gt;
			&lt;?desc_tema1?&gt;
	</pre>
      </li>
    </ul>
	
	<h2 class="bg-info"> <strong> Tema_Nivel2 </strong> </h2>
	<blockquote>
    <p>Descripción: 
		<footer>Los indicadores se encuentran organizados por 
niveles, dentro de este segundo nivel, se encuentran
				cultura, educación, hogares, natalidad y 
fecundidad, población, salud, vivienda y urbanización
		</footer> 
	</p>
	</blockquote>
    <ul>
	<p class="lead">
      <li><strong>URL</strong>: 
http://castellanosruedaedna.sun.fire/</li>
      <li><strong>Metodo HTTP</strong>: GET</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>
	 </p>
	<pre>	&lt;?xml version="1.0" encoding="UTF-8"?&gt;
			&lt;?catalogo?&gt;
			&lt;?id_tema2?&gt;id="confidencial"?&gt;
			&lt;?desc_tema2?&gt;
	</pre>
      </li>
    </ul>
	
	<h2 class="bg-info"> <strong> Tema_Nivel3 </strong> </h2>
	<blockquote>
    <p>Descripción: 
		<footer>Los indicadores se encuentran organizados por 
niveles, dentro de este tercer nivel, se encuentran
				caracteristicas educactivas de la 
población, caracteristicas culturales de la población, 
				caracteristicas de la viviendas, 
caracteristicas de los hogares, derechohabiencia y uso de servicios, 
				distribución por edad y sexo, natalidad, 
servicios y vienes de la vivienda, volumen y crecimiento.
		</footer> 
	</p>
	</blockquote>
    <ul>
	<p class="lead">
      <li><strong>URL</strong>: 
http://castellanosruedaedna.sun.fire/</li>
      <li><strong>Metodo HTTP</strong>: GET</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>
	 </p>
	<pre>	&lt;?xml version="1.0" encoding="UTF-8"?&gt;
			&lt;?catalogo?&gt;
			&lt;?id_tema3?&gt;id="confidencial"?&gt;
			&lt;?desc_tema3?&gt;
	</pre>
      </li>
    </ul>
  </body>
</html>
